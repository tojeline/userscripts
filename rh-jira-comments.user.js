// ==UserScript==
// @name        rh-jira-comments
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       none
// @version     1.4
// @author      Peter Krempa
// @description 2023/09/13
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    let seenCommentButton = false;

    /* Load all comments by shift-clicking the button once it appears. */
    function loadComments() {
        let loadButtons = document.querySelectorAll('.show-more-tab-items');

        if (loadButtons == null ||
            loadButtons.length == 0) {

            /* refocus the linked comment after we've loaded all comments */
            if (seenCommentButton &&
                window.location.hash.includes('#comment')) {
                window.location.href = window.location.hash;
            }

            return;
        }

        seenCommentButton = true;

        /* First look whether we're busy loading something because Jira doesn't
         * like to load both newer and older comments at the same time */
        for (const but of loadButtons) {
            if (but.hasAttribute('busy')) {
                /* defer clicking on anything while loading stuff */
                return;
            }
        }

        /* click the first of the load buttons */
        let ev = new MouseEvent("click", { bubbles: true, cancelable: false,  shiftKey: true, buttons: 1});
        loadButtons[0].dispatchEvent(ev);
    }

    let mut = new MutationObserver(loadComments);
    mut.observe(document.documentElement, { subtree: true, childList: true });

})();
