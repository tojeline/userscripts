// ==UserScript==
// @name        rh-jira-links
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description always display all links
// ==/UserScript==

GM.addStyle (`

    #show-more-links
    {
        display: none !important;
    }

    .collapsed-links-list,
    .collapsed-link
    {
        display: block !important;
    }
` );
