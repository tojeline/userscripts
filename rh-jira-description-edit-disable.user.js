// ==UserScript==
// @name        rh-jira-description-edit-disable
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @version     1.2
// @author      Peter Krempa
// @description disable editting of description by clicking anywhere into it (except for edit icon)
// @run-at      document-end
// ==/UserScript==

(function(){
    "use strict";

    /* helper for eating 'click' event from the description box so that it
     * doesn't trigger the editor except when the edit icon on the side is
     * clicked, or when the editor is already active */
    function description_edit_click_cancel(e) {
        if (e.srcElement.classList.contains('aui-iconfont-edit') ||
            document.querySelector('#description-form') != null) {
            return;
        }

        e.stopPropagation();
    }

    function install_description_edit_override() {
        let desc = document.querySelector('#description-val:not(.edit-click-overridden)');

        if (desc) {
            desc.addEventListener('click', description_edit_click_cancel, true);
            desc.classList.add('edit-click-overridden');
        }

        /* Remove the 'Click to edit' tooltip. It is loaded dynamically!! */
        let desctitle = document.querySelector('#description-val[title]');

        if (desctitle) {
            desctitle.removeAttribute('title');
        }
    }

    let mut = new MutationObserver(install_description_edit_override);
    mut.observe(document.documentElement, { subtree: true, childList: true });
    install_description_edit_override();

})();
