// ==UserScript==
// @name        rh-jira-monospace-comments
// @namespace   Violentmonkey Scripts
// @match       https://issues.redhat.com/*
// @grant       GM.addStyle
// @version     1.1
// @author      Peter Krempa
// @description 2023/09/13
// ==/UserScript==

GM.addStyle (`

/* Font stuff */

    /* Use browser-defined sans-serif font, instead of forced ones. */
    html
    {
        font-family: sans-serif;
    }

    /* Use monospace font for comment body and issue description. */
    .action-body,
    #description-val
    {
        font-family: monospace;
    }

    /* Remove 30 line size limitation for code blocks */
    pre
    {
        max-height: none !important;
    }
` );
