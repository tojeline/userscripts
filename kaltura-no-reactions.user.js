// ==UserScript==
// @name        kaltura-no-reactions
// @namespace   Violentmonkey Scripts
// @match       https://*.kaltura.com/*
// @grant       GM.addStyle
// @version     1.0
// @author      Peter Krempa
// @description remove annoying floating emoji reactions
// ==/UserScript==

GM.addStyle (`

.kaltura-ep-reactions-1-2-45
{
    display: none !important;
}
` );
